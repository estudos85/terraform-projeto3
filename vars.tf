variable "aws_region" {
  description = "AWS region"
  default     = "sa-east-1"
  type        = string
}

variable "access_key" {
  description = "Your AWS Access key"
  default     = "AKIAQQYE3XB5Q3XKD36U"
  type        = string
}

variable "secret_key" {
  description = "Your AWS Secret key"
  default     = "o2ktkM3iLa7/J7wTzfVvcx/jFf4gENvbJ1mqYhbe"
  type        = string
}

variable "sg_cidrs" {
  type        = list (string)
  default     = ["179.54.197.50/32","179.54.197.51/32"]
}

variable "amis" {
  type        = map
  default     = {
    us-east-1 = "ami-08c40ec9ead489470"
    sa-east-1 = "ami-04b3c23ec8efcc2d6"
  }
}

# Input variable definitions

variable "vpc_name" {
  description = "Nome da minha VPC"
  type        = string
  default     = "Emxemplo de vpc"
}

variable "vpc_cidr" {
  description = "CIDR block for vpc"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_azs" {
  description = "AZS for vpc"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "vpc_private_subnets" {
  description = "Private subnets for vpc"
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "vpc_public_subnets" {
  description = "Public subnets for vpc"
  type        = list(string)
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
}

variable "vpc_enable_nat_gateway" {
  description = "Enable getway for vpc"
  type        = bool
  default     = true
}

variable "vpc_enable_vpn_gateway" {
  description = "Enable vpn getway for vpc"
  type        = bool
  default     = true
}

variable "vpc_tags" {
  description   = "vpc_tags"
  type          = map(string)
  default       = {
    terraform   = "true"
    Environment = "dev"
  }
}
